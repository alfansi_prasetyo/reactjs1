import React from 'react';
import './TugasForm.css';

class TugasForm extends React.Component{
    render(){
        return(
            <form className="formGroup">
                <input type="text" className="input" placeholder={this.props.placeholder}></input>
            </form>
        )
    }
}

export default TugasForm
