import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './App';
// import Greetings from './components/Greetings';
import Tugas from './components/Tugas';
import TugasForm from './components/TugasForm';
import TugasButton from './components/TugasButton';
import * as serviceWorker from './serviceWorker';

// const element = <h1>Hello World!!!</h1>;

// const element_by_jsx = (
//   <h3>Halo Dunia</h3>
// );

// const element_by_react = React.createElement (
//   'h1', {}, 'Halo, Dunia!!!'
// );

// const name = 'Izmi';
// const tahun_lahir = '1998';
// const element_by_ex = (
//     <div>
//       <h1>Halo, {name}.</h1>
//       <h2>Kelahiran tahun {tahun_lahir}</h2>
//       <h3>Dengan umur {2020-tahun_lahir}</h3>
//     </div>
//     );


// const element_att = React.createElement (
//   'h1', {className: 'greeting'}, 'Halo, Dunia!!!'
// );

// const element_w_child = (
//   <div>
//       <h1>Lorem Ipsum</h1>
//       <p>There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...</p>
//       <input type="text" placeholder="input"/>
//   </div>
// );

// const element_wo_child = <input type="text" placeholder="Ini gak ada child"/>;

ReactDOM.render(
  <React.StrictMode>
    {
    /* {<App /> */
    /* <Greetings nama="Izmi" />  */
    /* <Welcome nama="Habiba"/> */
    <div>
    <Tugas name="Izmi" />
    <TugasForm placeholder="Username" />
    <TugasForm placeholder="Password" />
    <div className="flex-container">
    <TugasButton button="Login" />
    <TugasButton button="Cancel" />
    </div>
    </div>
    }
  </React.StrictMode>
  // element
  // element_by_jsx
  // element_by_react
  // element_by_ex
  // element_att
  // element_w_child
  // element_wo_child
  ,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
